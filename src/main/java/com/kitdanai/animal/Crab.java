/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kitdanai.animal;

/**
 *
 * @author ADMIN
 */
public class Crab extends AquaticAnimal{
    private String name;
    private int numOfLeg;

    public Crab(String name) {
        super("Crab",8);
        this.name = name;
        this.numOfLeg=numOfLeg;
    }

    @Override
    public void swim() {
        System.out.println("Crab: "+this.name+" swim");
    }

    @Override
    public void eat() {
       System.out.println("Crab: "+this.name+" eat");
    }

    @Override
    public void walk() {
        System.out.println("Crab: "+this.name+" walk");
    }

    @Override
    public void speak() {
       System.out.println("Crab: "+this.name+" cannot speak");
    }

    @Override
    public void sleep() {
        System.out.println("Crab: "+this.name+" sleep");
    }
}
