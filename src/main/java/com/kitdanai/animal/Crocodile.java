/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kitdanai.animal;

/**
 *
 * @author ADMIN
 */
public class Crocodile extends Reptile{
    private String name;

    public Crocodile(String name) {
        super("Crocodile", 4);
        this.name=name;
    }
    
    @Override
    public void crawl() {
        System.out.println("Crcodile: "+ this.name + " crawl ");
    }

    @Override
    public void eat() {
        System.out.println("Crcodile: "+ this.name + " eat ");
    }

    @Override
    public void walk() {
       System.out.println("Crcodile: "+ this.name + " cannot walk ");
    }

    @Override
    public void speak() {
        System.out.println("Crcodile: "+ this.name + " speak ");
    }

    @Override
    public void sleep() {
        System.out.println("Crcodile: "+ this.name + " sleep ");
    }
    
}
