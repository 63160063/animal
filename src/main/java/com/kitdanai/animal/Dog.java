/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kitdanai.animal;

/**
 *
 * @author ADMIN
 */
public class Dog extends LandAnimal{
    private String name;

    public Dog(String name) {
        super("Dog", 4);
        this.name = name;
    }

    @Override
    public void run() {
        System.out.println("Dog: "+this.name+" run ");
    }

    @Override
    public void eat() {
        System.out.println("Dog: "+this.name+" eat ");
    }

    @Override
    public void walk() {
        System.out.println("Dog: "+this.name+" walk ");
    }

    @Override
    public void speak() {
        System.out.println("Dog: "+this.name+" speak ");
    }

    @Override
    public void sleep() {
        System.out.println("Dog: "+this.name+" sleep ");
    }
    
}
