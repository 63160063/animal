/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kitdanai.animal;

/**
 *
 * @author ADMIN
 */
public class TestAnimal {
    public static void main(String[] args) {
        Human h1 = new Human("Dang");
        h1.eat();
        h1.walk();
        h1.run();
        System.out.println("h1 is animal ?"+ (h1 instanceof Animal));
        System.out.println("h1 is land animal ?"+ (h1 instanceof LandAnimal));
        
        Animal a1 = h1;
        System.out.println("a1 is land animal ?"+ (a1 instanceof Animal));
        System.out.println("a1 is reptile animal ?"+ (a1 instanceof Reptile));
        
        Crocodile c1 = new Crocodile("Gigi");
        c1.crawl();
        c1.eat();
        c1.sleep();
        
        Animal a2 = c1;
        System.out.println("a2 is reptile animal ? "+(a2 instanceof Reptile));
        System.out.println("a2 is  animal ? "+(a2 instanceof Animal));
        System.out.println("a2 is poultry animal ? "+(a2 instanceof Poultry));
        
        Snake s1 = new Snake("Scarlet");
        s1.eat();
        s1.crawl();
        
        Animal a3 = s1;
        System.out.println("a3 is reptile animal ? "+(a3 instanceof Reptile));
        System.out.println("a3 is  animal ? "+(a3 instanceof Animal));
        System.out.println("a3 is poultry animal ? "+(a3 instanceof Poultry));
        
        Cat c2 = new Cat("panda");
        c2.eat();
        c2.run();
        c2.sleep();
        c2.speak();
        
        Animal a4 = c2;
        System.out.println("a4 is aquatic animal ? "+(a3 instanceof AquaticAnimal));
        System.out.println("a4 is  animal ? "+(a3 instanceof Animal));
        System.out.println("a4 is land animal ? "+(a3 instanceof LandAnimal));
        
        Dog d1 = new Dog("khawjao");
        d1.eat();
        d1.run();
        d1.sleep();
        d1.speak();
        
        Animal a5 = d1;
        System.out.println("a5 is aquatic animal ? "+(a5 instanceof AquaticAnimal));
        System.out.println("a5 is  animal ? "+(a5 instanceof Animal));
        System.out.println("a5 is land animal ? "+(a5 instanceof LandAnimal));
        
        Fish f1 = new Fish("Dory");
        f1.swim();
        f1.sleep();
        f1.walk();
        
        Animal a6 = f1;
        System.out.println("a6 is aquatic animal ? "+(a6 instanceof AquaticAnimal));
        System.out.println("a6 is  animal ? "+(a6 instanceof Animal));
        
        Crab c3 = new Crab("Nan");
        c3.eat();
        c3.swim();
        
         Animal a7 = c3;
        System.out.println("a7 is aquatic animal ? "+(a7 instanceof AquaticAnimal));
        System.out.println("a7 is  animal ? "+(a7 instanceof Animal));
        
        Bat b1 = new Bat("Salime");
        b1.eat();
        b1.fly();
        b1.sleep();
        
        Animal a8 = b1;
        System.out.println("a8 is aquatic animal ? "+(a8 instanceof AquaticAnimal));
        System.out.println("a8 is  animal ? "+(a8 instanceof Animal));
        System.out.println("a8 is  poultry animal ? "+(a8 instanceof Poultry));
        
        Bird b2 = new Bird("Marin");
        b2.eat();
        b2.fly();
        b2.sleep();
        
        Animal a9 = b2;
        System.out.println("a9 is aquatic animal ? "+(a9 instanceof AquaticAnimal));
        System.out.println("a9 is  animal ? "+(a9 instanceof Animal));
        System.out.println("a9 is  poultry animal ? "+(a9 instanceof Poultry));
    }
}
